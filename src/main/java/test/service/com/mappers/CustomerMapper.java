package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Customer;
import test.service.com.extendedEntities.ExtendedCustomer;

public interface CustomerMapper {
	@Select("select id as id, fullName as fullName, phoneNumber as phoneNumber from customer where id=#{customerId}")
	Customer findCustomerById(Integer customerId);

	@Select("select id as id, fullName as fullName, phoneNumber as phoneNumber from customer")
	List<Customer> findAllCustomers();
	
	@Select("select customer.id as id, fullName as fullName, phoneNumber as phoneNumber, customCount as customCount from customer,"
			+ "(select customer_id, count(*) customCount from custom group by customer_id) countTable "
			+ "where customer_id=customer.id order by customCount")
	List<ExtendedCustomer> findSortedCustomers();
	
	@Select("select id as id, fullName as fullName, phoneNumber as phoneNumber from customer "
			+ "where fullName=#{fullName} and phoneNumber=#{phoneNumber}")
	Customer findThisCustomer(Customer customer);
	
	@Delete("delete from customer where id = #{customerId}")
	void delCustomerById(Integer customerId);
	
	@Insert("insert into customer(fullName, phoneNumber) values (#{fullName}, #{phoneNumber})")
	int addCustomer(Customer customer);
	
	@Update("update customer set fullName = #{fullName}, phoneNumber = #{phoneNumber}  where id = #{id}")
	void updateCustomer(Customer customer);
}
