package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Master;
import test.service.com.extendedEntities.ExtendedMaster;

public interface MasterMapper {	
	@Select("select id as id, fullName as fullName from master")
	List<Master> findAllMasters();
	
	@Select("select id as id, fullName as fullName, customCount as customCount from master,"
			+ "(select master_id, count(*) customCount from custom group by master_id) countTable "
			+ "where master_id=id order by customCount")
	List<ExtendedMaster> findAllSortedMasters();
	
	@Select("select id as id, fullName as fullName from master where id=#{masterId}")
	Master findMasterById(Integer masterId);
	
	@Delete("delete from master where id = #{masterId}")
	void delMasterById(Integer masterId);
	
	@Insert("insert into master (fullName) values (#{fullName})")
	int addMaster(Master master);
	
	@Update("update master set fullName = #{fullName} where id = #{id}")
	void updateMaster(Master master);
}
