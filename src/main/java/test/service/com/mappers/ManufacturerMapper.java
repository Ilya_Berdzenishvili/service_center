package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Manufacturer;

public interface ManufacturerMapper {
	@Select("select id as id, name as name from manufacturer where id=#{manufacturerId}")
	Manufacturer findManufacturerById(Integer manufacturerId);

	@Select("select id as id, name as name from manufacturer")
	List<Manufacturer> findAllManufacturers();
	
	@Delete("delete from manufacturer where id = #{manufacturerId}")
	void delManufacturerById(Integer manufacturerId);
	
	@Insert("insert into manufacturer(name) values (#{name})")
	int addManufacturer(Manufacturer manufacturer);
	
	@Update("update manufacturer set name = #{name} where id = #{id}")
	void updateManufacturer(Manufacturer manufacturer);

}
