package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Device;

public interface DeviceMapper {
	@Select("select id as id, type as type from device where id=#{deviceId}")
	Device findDeviceById(Integer deviceId);

	@Select("select id as id, type as type from device")
	List<Device> findAllDevices();
	
	@Delete("delete from device where id = #{deviceId}")
	void delDeviceById(Integer deviceId);
	
	@Insert("insert into device(type) values (#{type})")
	int addDevice(Device device);
	
	@Update("update device set type = #{type} where id = #{id}")
	void updateDevice(Device device);
}
