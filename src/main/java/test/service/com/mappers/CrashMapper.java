package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Crash;

public interface CrashMapper {
	@Select("select id as id, type as type from crash where id=#{crashId}")
	Crash findCrashById(Integer crashId);

	@Select("select id as id, type as type from crash")
	List<Crash> findAllCrashes();
	
	@Delete("delete from crash where id = #{crashId}")
	void delCrashById(Integer crashId);
	
	@Insert("insert into crash(type) values (#{type})")
	int addCrash(Crash crash);
	
	@Update("update crash set type = #{type} where id = #{id}")
	void updateCrash(Crash crash);
}
