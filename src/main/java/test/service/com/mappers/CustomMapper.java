package test.service.com.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import test.service.com.entities.Custom;
import test.service.com.extendedEntities.ExtendedCustom;

public interface CustomMapper {
	@Select("select id as id, date as date,  deadline as deadline, state as state, fullDescription as fullDescription, guarantee as guarantee,"
			+ "customer_id as customerId, device_id as deviceId, manufacturer_id as manufacturerId, crash_id as crashId, master_id as masterId "
			+ "from custom where id = #{customId}")
	Custom findCustomById(Integer customId);
	
	@Select("select custom.id as id, date as date,  deadline as deadline, state as state, fullDescription as fullDescription, guarantee as guarantee,"
			+ "customer_id as customerId, device_id as deviceId, manufacturer_id as manufacturerId, crash_id as crashId, master_id as masterId,"
			+ "master.fullName as masterFullName, customer.fullName as customerFullName, customer.phoneNumber as customerPhoneNumber,"
			+ "device.type as deviceType, manufacturer.name as manufacturerName, crash.type as crashType "
			+ "from custom,master,customer,device,manufacturer,crash where master_id=master.id and manufacturer_id=manufacturer.id "
			+ "and device_id=device.id and crash_id=crash.id and customer_id=customer.id")
	List<ExtendedCustom> findAllCustoms();
	
	@Select("select custom.id as id, date as date,  deadline as deadline, state as state, fullDescription as fullDescription, guarantee as guarantee,"
			+ "customer_id as customerId, device_id as deviceId, manufacturer_id as manufacturerId, crash_id as crashId, master_id as masterId,"
			+ "master.fullName as masterFullName, customer.fullName as customerFullName, customer.phoneNumber as customerPhoneNumber,"
			+ "device.type as deviceType, manufacturer.name as manufacturerName, crash.type as crashType "
			+ "from custom,master,customer,device,manufacturer,crash where state=#{state} and master_id=master.id "
			+ "and manufacturer_id=manufacturer.id and device_id=device.id and crash_id=crash.id and customer_id=customer.id")
	List<ExtendedCustom> findCustomsWithState(String state);
	
	@Delete( "delete from custom where id=#{customId}" )
	void delCustomById( Integer customId );
	
	@Insert("insert into custom(date,deadline,state,guarantee,fullDescription,master_id,customer_id,device_id,crash_id,manufacturer_id) "
			+ "values(#{date},#{deadline},#{state},#{guarantee},#{fullDescription},#{masterId},#{customerId},#{deviceId},#{crashId},#{manufacturerId})")
	void addCustom(Custom custom);
	
	@Update("update custom set date = #{date}, deadline = #{deadline}, state = #{state}, guarantee = #{guarantee},fullDescription = #{fullDescription},"
			+ "master_id = #{masterId}, customer_id = #{customerId}, manufacturer_id = #{manufacturerId}, device_id = #{deviceId},"
			+ "crash_id = #{crashId} where id = #{id}")
	void updateCustom(Custom custom);
}
