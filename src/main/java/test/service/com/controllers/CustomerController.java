package test.service.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Customer;
import test.service.com.extendedEntities.ExtendedCustomer;
import test.service.com.services.CustomerService;

@Controller
public class CustomerController {
	@Autowired
	CustomerService customerService;
	
	/**
	 * render all customers
	 */
	@RequestMapping(value="/customers",method=RequestMethod.GET)
	public String renderCustomers(Model model)
	{
		List<Customer> customers=customerService.findAllCustomers();
		model.addAttribute("customers", customers);
		
		boolean isSorted = false;
		model.addAttribute("isSorted", isSorted);
		
		return "customerViews/customers";
	}
	
	/**
	 * render all customers with custom count
	 */
	@RequestMapping(value="/sortedCustomers",method=RequestMethod.GET)
	public String renderSortedCustomers(Model model)
	{
		List<ExtendedCustomer> customers=customerService.findSortedCustomers();
		model.addAttribute("customers", customers);

		boolean isSorted = true;
		model.addAttribute("isSorted", isSorted);
		
		return "customerViews/customers";
	}
	
	/**
	 * delete customer
	 */
	@RequestMapping(value="/delCustomer",method=RequestMethod.GET)
	public String delCustomer(@RequestParam("customerId") int customerId)
	{		
		customerService.delCustomerById(customerId);
		
		return "redirect:customers";
	}
}
