package test.service.com.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Crash;
import test.service.com.entities.Custom;
import test.service.com.entities.Customer;
import test.service.com.entities.Device;
import test.service.com.entities.Manufacturer;
import test.service.com.entities.Master;
import test.service.com.extendedEntities.ExtendedCustom;
import test.service.com.services.CrashService;
import test.service.com.services.CustomService;
import test.service.com.services.CustomerService;
import test.service.com.services.DeviceService;
import test.service.com.services.ManufacturerService;
import test.service.com.services.MasterService;

@Controller
public class CustomController 
{
	@Autowired
	private CustomService customService;
	
	@Autowired
	private MasterService masterService;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private ManufacturerService manufacturerService;
	
	@Autowired
	private CrashService crashService;
	
	@Autowired
	private CustomerService customerService;
	
	/**
	 * render all customs
	*/
	@RequestMapping(value="/customs",method=RequestMethod.GET)
	public String renderCustoms(Model model)
	{
		String typeOfCustoms = "All";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> customs = customService.findAllCustoms();
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render completed customs
	*/
	@RequestMapping(value="/completedCustoms",method=RequestMethod.GET)
	public String renderCompletedCustoms(Model model)
	{
		String typeOfCustoms = "Completed";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> customs = customService.findCustomsWithState(typeOfCustoms);
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render not yet completed customs
	*/
	@RequestMapping(value="/notCompletedCustoms",method=RequestMethod.GET)
	public String renderNonCompletedCustoms(Model model)
	{
		String typeOfCustoms = "In process";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> customs = customService.findCustomsWithState(typeOfCustoms);
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render faulty customs
	*/
	@RequestMapping(value="/faultyCustoms",method=RequestMethod.GET)
	public String renderFaultyCustoms(Model model)
	{
		String typeOfCustoms = "Faulty";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> customs = customService.findCustomsWithState(typeOfCustoms);
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render new customs
	*/
	@RequestMapping(value="/newCustoms",method=RequestMethod.GET)
	public String renderNewCustoms(Model model)
	{
		String typeOfCustoms = "New";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> customs = customService.findCustomsWithState(typeOfCustoms);
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render failed customs
	*/
	@RequestMapping(value="/failedCustoms",method=RequestMethod.GET)
	public String renderFailedCustoms(Model model)
	{
		String typeOfCustoms = "Failed";
		model.addAttribute("typeOfCustoms",typeOfCustoms);
		
		List<ExtendedCustom> temp = customService.findCustomsWithState("In process");
		List<ExtendedCustom> customs = new LinkedList<ExtendedCustom>();
		
		for (ExtendedCustom custom : temp) 
			if (custom.getDeadline().compareTo(new Date())<=0)
				customs.add(custom);
		
		model.addAttribute("customs", customs);
		
		return "customViews/customs";
	}
	
	/**
	 * render form for adding custom
	*/
	@RequestMapping(value="/addCustom",method=RequestMethod.GET)
	public String rendrAddForm(Model model)
	{		
		Custom custom = new Custom();
		model.addAttribute("custom", custom);
		
		List<Master> masters = masterService.findAllMasters();
		model.addAttribute("masters",masters);
		
		List<Device> devices = deviceService.findAllDevices();
		model.addAttribute("devices",devices);

		List<Manufacturer> manufacturers = manufacturerService.findAllManufacturers();
		model.addAttribute("manufacturers",manufacturers);
		
		List<Crash> crashes = crashService.findAllCrashes();
		model.addAttribute("crashes",crashes);
		
		boolean flag = true;
		model.addAttribute("isForAdd",flag);
		
		return "customViews/addCustomForm";
	}
	
	/**
	 * add custom
	*/
	@RequestMapping(value="/addCustom",method=RequestMethod.POST)
	public String addCustom(@ModelAttribute("custom") Custom custom,
			@RequestParam("term") int term,
			@RequestParam("customerFullName") String customerFullName,
			@RequestParam("customerPhoneNumber") String customerPhoneNumber)
	{	
		Customer customer = new Customer();
		customer.setFullName(customerFullName);
		customer.setPhoneNumber(customerPhoneNumber);
		
		if(customerService.findThisCustomer(customer)==null)
			customerService.addCustomer(customer);

		custom.setCustomerId(customerService.findThisCustomer(customer).getId());
		
		Date date=new Date();
		custom.setDate(date);
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, term);
		custom.setDeadline(c.getTime());
		
		custom.setState("New");
		
		customService.addCustom(custom);
		
		return "redirect:customs";
	}
	
	/**
	 *render form for edit custom
	*/
	@RequestMapping(value="/editCustom",method=RequestMethod.GET)
	public String rendEditForm(Model model,@RequestParam("customId") int customId)
	{	
		Custom custom = customService.findCustomById(customId);
		model.addAttribute("custom", custom);
		
		List<Master> masters = masterService.findAllMasters();
		model.addAttribute("masters",masters);
		
		List<Device> devices = deviceService.findAllDevices();
		model.addAttribute("devices",devices);

		List<Manufacturer> manufacturers = manufacturerService.findAllManufacturers();
		model.addAttribute("manufacturers",manufacturers);
		
		List<Crash> crashes = crashService.findAllCrashes();
		model.addAttribute("crashes",crashes);
		
		Customer customer = customerService.findCustomerById(custom.getCustomerId());
		model.addAttribute("customerFullName", customer.getFullName());
		model.addAttribute("customerPhoneNumber", customer.getPhoneNumber());
		
		boolean flag = false;
		model.addAttribute("isForAdd",flag);
		
		return "customViews/addCustomForm";
	}
	
	/**
	 * edit custom
	*/
	@RequestMapping(value="/editCustom",method=RequestMethod.POST)
	public String editCustom(@ModelAttribute("custom") Custom custom,
			@RequestParam("term") int term,
			@RequestParam("customerFullName") String customerFullName,
			@RequestParam("customerPhoneNumber") String customerPhoneNumber)
	{	
		Customer customer = new Customer();
		customer.setFullName(customerFullName);
		customer.setPhoneNumber(customerPhoneNumber);
		
		if(customerService.findThisCustomer(customer)==null)
			customerService.addCustomer(customer);

		custom.setCustomerId(customerService.findThisCustomer(customer).getId());
		
		Date date=new Date();
		custom.setDate(date);
			
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, term);
		custom.setDeadline(c.getTime());
	
		customService.updateCustom(custom);
		
		return "redirect:customs";
	}
	
	/**
	 * delete custom
	*/
	@RequestMapping(value="/delCustom",method=RequestMethod.GET)
	public String delCustom(@RequestParam("customId") int customId)
	{		
		customService.delCustomById(customId);
		
		return "redirect:customs";
	}
}
