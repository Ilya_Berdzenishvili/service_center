package test.service.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Manufacturer;
import test.service.com.services.ManufacturerService;

@Controller
public class ManufactuerController {
	@Autowired
	ManufacturerService manufacturerService;
	
	/**
	 * render all manufacturers
	 */
	@RequestMapping(value="/manufacturers",method=RequestMethod.GET)
	public String renderManufacturers(Model model)
	{			
		List<Manufacturer> manufacturers = manufacturerService.findAllManufacturers();
		model.addAttribute("manufacturers", manufacturers);
		
		return "manufacturerViews/manufacturers";
	}
	
	/**
	 * render form for adding manufacturer
	 */
	@RequestMapping(value="/addManufacturer",method=RequestMethod.GET)
	public String rendrAddForm()
	{		
		return "manufacturerViews/addManufacturerForm";
	}
	
	/**
	 * add manufacturer
	 */
	@RequestMapping(value="/addManufacturer",method=RequestMethod.POST)
	public String addManufacturer(@ModelAttribute("manufacturer") Manufacturer manufacturer)
	{
		manufacturerService.addManufacturer(manufacturer);
		
		return "redirect:manufacturers";
	}
	
	/**
	 * delete manufacturer
	 */
	@RequestMapping(value="/delManufacturer",method=RequestMethod.GET)
	public String delManufacturer(@RequestParam("manufacturerId") int manufacturerId)
	{		
		manufacturerService.delManufacturerById(manufacturerId);
		
		return "redirect:manufacturers";
	}
}
