package test.service.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Device;
import test.service.com.services.DeviceService;

@Controller
public class DeviceController {
	@Autowired
	private DeviceService deviceService;
	
	/**
	 * render all devices
	 */
	@RequestMapping(value="/devices",method=RequestMethod.GET)
	public String renderDevices(Model model)
	{			
		List<Device> devices = deviceService.findAllDevices();
		model.addAttribute("devices", devices);
		
		return "deviceViews/devices";
	}
	
	/**
	 * render form for adding device
	 */
	@RequestMapping(value="/addDevice",method=RequestMethod.GET)
	public String rendrAddForm()
	{		
		return "deviceViews/addDeviceForm";
	}
	
	/**
	 * add device
	 */
	@RequestMapping(value="/addDevice",method=RequestMethod.POST)
	public String addDevice(@ModelAttribute("device") Device device)
	{
		deviceService.addDevice(device);
		
		return "redirect:devices";
	}
	
	/**
	 * delete device
	 */
	@RequestMapping(value="/delDevice",method=RequestMethod.GET)
	public String delDevice(@RequestParam("deviceId") int deviceId)
	{		
		deviceService.delDeviceById(deviceId);
		
		return "redirect:devices";
	}
}
