package test.service.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Crash;
import test.service.com.services.CrashService;

@Controller
public class CrashController {
	@Autowired
	CrashService crashService;

	/**
	 * render all crashes
	 */
	@RequestMapping(value="/crashes",method=RequestMethod.GET)
	String renderCrashes(Model model)
	{
		List<Crash> crashes = crashService.findAllCrashes();
		model.addAttribute("crashes",crashes);
		
		return "crashViews/crashes";
	}
	
	/**
	 * render form for adding crash
	 */
	@RequestMapping(value="/addCrash",method=RequestMethod.GET)
	public String rendrAddForm()
	{		
		return "crashViews/addCrashForm";
	}
	
	/**
	 * add crash
	 */
	@RequestMapping(value="/addCrash",method=RequestMethod.POST)
	public String addManufacturer(@ModelAttribute("crash") Crash crash)
	{
		crashService.addCrash(crash);
		
		return "redirect:crashes";
	}
	
	/**
	 * delete crash
	 */
	@RequestMapping(value="/delCrash",method=RequestMethod.GET)
	public String delManufacturer(@RequestParam("crashId") int crashId)
	{		
		crashService.delCrashById(crashId);
		
		return "redirect:crashes";
	}
}
