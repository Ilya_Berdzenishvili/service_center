package test.service.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import test.service.com.entities.Master;
import test.service.com.extendedEntities.ExtendedMaster;
import test.service.com.services.CustomService;
import test.service.com.services.MasterService;

@Controller
public class MasterController 
{
	@Autowired
	private CustomService customService;
	
	@Autowired
	private MasterService masterService;
	
	/**
	 * render all masters
	 */
	@RequestMapping(value="/masters",method=RequestMethod.GET)
	public String renderMasters(Model model)
	{	
		boolean flag = false;
		model.addAttribute("isSorted",flag);
		
		List<Master> masters = masterService.findAllMasters();
		model.addAttribute("masters", masters);
		
		return "masterViews/masters";
	}
	
	/**
	 * render all masters with custom count
	 */
	@RequestMapping(value="/sortedMasters",method=RequestMethod.GET)
	public String renderSortedMasters(Model model)
	{	
		boolean flag = true;
		model.addAttribute("isSorted",flag);
		
		List<ExtendedMaster> masters = masterService.findAllSortedMasters();
		model.addAttribute("masters", masters);
		
		return "masterViews/masters";
	}
	
	/**
	 * render form for adding master
	 */
	@RequestMapping(value="/addMaster",method=RequestMethod.GET)
	public String rendrAddForm()
	{		
		return "masterViews/addMasterForm";
	}
	
	/**
	 * add master
	 */
	@RequestMapping(value="/addMaster",method=RequestMethod.POST)
	public String addMaster(@ModelAttribute("master") Master master)
	{
		masterService.addMaster(master);
		
		return "redirect:masters";
	}
	
	/**
	 * delete master
	 */
	@RequestMapping(value="/delMaster",method=RequestMethod.GET)
	public String delMaster(@RequestParam("masterId") int masterId)
	{		
		masterService.delMasterById(masterId);
		
		return "redirect:masters";
	}	
}
