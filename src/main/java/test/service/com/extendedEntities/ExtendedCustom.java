package test.service.com.extendedEntities;

import test.service.com.entities.Custom;

/**
 * Serves to store responsible master's full name
 */
public class ExtendedCustom extends Custom {
	private String masterFullName;
	
	private String customerFullName;
	
	private String customerPhoneNumber;
	
	private String deviceType;
	
	private String manufacturerName;
	
	private String crashType;

	public String getMasterFullName() {
		return masterFullName;
	}

	public void setMasterFullName(String masterFullName) {
		this.masterFullName = masterFullName;
	}

	public String getCustomerFullName() {
		return customerFullName;
	}

	public void setCustomerFullName(String customerFullName) {
		this.customerFullName = customerFullName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getCrashType() {
		return crashType;
	}

	public void setCrashType(String crashType) {
		this.crashType = crashType;
	}
	


}
