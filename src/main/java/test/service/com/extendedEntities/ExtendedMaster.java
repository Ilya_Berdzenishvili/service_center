package test.service.com.extendedEntities;

import test.service.com.entities.Master;

/**
 * Serves to store number of customs for every master
 */
public class ExtendedMaster extends Master{	
	private Integer customCount;

	public Integer getCustomCount() {
		return customCount;
	}

	public void setCustomCount(int customCount) {
		this.customCount = customCount;
	}

}
