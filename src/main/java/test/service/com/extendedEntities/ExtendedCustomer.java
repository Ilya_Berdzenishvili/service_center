package test.service.com.extendedEntities;

import test.service.com.entities.Customer;

public class ExtendedCustomer extends Customer {
	private Integer customCount;

	public Integer getCustomCount() {
		return customCount;
	}

	public void setCustomCount(Integer customCount) {
		this.customCount = customCount;
	}
}
