package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Crash;
import test.service.com.mappers.CrashMapper;

@Service
@Transactional
public class CrashService {
	@Autowired
	CrashMapper crashMapper;
	
	public Crash findCrashById(Integer crashId)
	{
		return crashMapper.findCrashById(crashId);
	}
	
	public List<Crash> findAllCrashes()
	{
		return crashMapper.findAllCrashes();
	}
	
	public void delCrashById(Integer crashId)
	{
		crashMapper.delCrashById(crashId);
	}
	
	public void addCrash(Crash crash)
	{
		crashMapper.addCrash(crash);
	}
	
	public void updateCrash(Crash crash)
	{
		crashMapper.updateCrash(crash);
	}	
}
