package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Device;
import test.service.com.mappers.DeviceMapper;

@Service
@Transactional
public class DeviceService {
	@Autowired
	DeviceMapper deviceMapper;
	
	public Device findDeviceById(Integer manufacturerId)
	{
		return deviceMapper.findDeviceById(manufacturerId);
	}
	
	public List<Device> findAllDevices()
	{
		return deviceMapper.findAllDevices();
	}
	
	public void delDeviceById(Integer deviceId)
	{
		deviceMapper.delDeviceById(deviceId);
	}
	
	public void addDevice(Device device)
	{
		deviceMapper.addDevice(device);
	}
	
	public void updateDevice(Device device)
	{
		deviceMapper.updateDevice(device);
	}	
}
