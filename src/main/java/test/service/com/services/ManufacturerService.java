package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Manufacturer;
import test.service.com.mappers.ManufacturerMapper;

@Service
@Transactional
public class ManufacturerService {
	@Autowired
	ManufacturerMapper manufacturerMapper;
	
	public Manufacturer findManufacturerById(Integer manufacturerId)
	{
		return manufacturerMapper.findManufacturerById(manufacturerId);
	}
	
	public List<Manufacturer> findAllManufacturers()
	{
		return manufacturerMapper.findAllManufacturers();
	}
	
	public void delManufacturerById(Integer manufacturerId)
	{
		manufacturerMapper.delManufacturerById(manufacturerId);
	}
	
	public void addManufacturer(Manufacturer manufacturer)
	{
		manufacturerMapper.addManufacturer(manufacturer);
	}
	
	public void updateManufacturer(Manufacturer manufacturer)
	{
		manufacturerMapper.updateManufacturer(manufacturer);
	}
}
