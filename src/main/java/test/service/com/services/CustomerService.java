package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Customer;
import test.service.com.extendedEntities.ExtendedCustomer;
import test.service.com.mappers.CustomerMapper;

@Service
@Transactional
public class CustomerService {
	@Autowired
	CustomerMapper customerMapper;

	public Customer findCustomerById(Integer customerId)
	{
		return customerMapper.findCustomerById(customerId);
	}
	
	public List<Customer> findAllCustomers()
	{
		return customerMapper.findAllCustomers();
	}
	
	public List<ExtendedCustomer> findSortedCustomers()
	{
		return customerMapper.findSortedCustomers();
	}
	
	public Customer findThisCustomer(Customer customer)
	{
		return customerMapper.findThisCustomer(customer);
	}
	
	public void delCustomerById(Integer customerId)
	{
		customerMapper.delCustomerById(customerId);
	}
	
	public void addCustomer(Customer customer)
	{
		customerMapper.addCustomer(customer);
	}
	
	public void updateCustomer(Customer customer)
	{
		customerMapper.updateCustomer(customer);
	}
}
