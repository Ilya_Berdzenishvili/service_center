package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Custom;
import test.service.com.extendedEntities.ExtendedCustom;
import test.service.com.mappers.CustomMapper;

@Service
@Transactional
public class CustomService {
	@Autowired
	CustomMapper customMapper;
	
	public Custom findCustomById(Integer customId)
	{
		return customMapper.findCustomById(customId);
	}
	
	public List<ExtendedCustom> findAllCustoms()
	{
		return customMapper.findAllCustoms();
	}
	
	public List<ExtendedCustom> findCustomsWithState(String state)
	{
		return customMapper.findCustomsWithState(state);
	}
	
	public void delCustomById(Integer customId)
	{
		customMapper.delCustomById(customId);
	}
	
	public void addCustom(Custom custom)
	{
		customMapper.addCustom(custom);
	}
	
	public void updateCustom(Custom custom)
	{
		customMapper.updateCustom(custom);
	}
}
