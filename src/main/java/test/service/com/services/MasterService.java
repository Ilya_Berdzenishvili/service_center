package test.service.com.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.service.com.entities.Master;
import test.service.com.extendedEntities.ExtendedMaster;
import test.service.com.mappers.MasterMapper;

@Service
@Transactional
public class MasterService {
	@Autowired
	MasterMapper masterMapper;
	
	public Master findMasterById(Integer masterId)
	{
		return masterMapper.findMasterById(masterId);
	}
	
	public List<Master> findAllMasters()
	{
		return masterMapper.findAllMasters();
	}
	
	public List<ExtendedMaster> findAllSortedMasters()
	{
		return masterMapper.findAllSortedMasters();
	} 
		
	public void delMasterById(Integer masterId)
	{
		masterMapper.delMasterById(masterId);
	}
	
	public int addMaster(Master master)
	{
		return masterMapper.addMaster(master);
	}
	
	public void updateMaster(Master master)
	{
		masterMapper.updateMaster(master);
	}		
}
