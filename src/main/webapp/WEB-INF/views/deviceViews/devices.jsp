<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of devices</title>
</head>
<body>
	<h3>List of devices</h3>
	<hr align="left" size="1" color="#ff0000" /><br> 
	<table border="1" bordercolor="black" rules="all">
		<tr> 
			<td>Type</td>
			<td></td>
	 	</tr>
		<c:forEach var="device" items="${devices}"> 
			<tr>
			    <td><c:out value="${device.type}"/></td>
				<td><form action="delDevice" method="get">
					<input type="image" src="<c:url value="/resources/images/delIcon.jpg"/>"/>
					<input type="hidden" name="deviceId" value="${device.id}">
				</form></td>
    		</tr>
		</c:forEach>  
	</table>
	<p><a href="addDevice"> Add device </a></p>	 
	<p><a href="*"> Home page </a></p>
</body>
</html>