<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of masters</title>
</head>
<body> 
	<c:if test="${!isSorted}">
		<h3>List of masters</h3>
	</c:if>
	<c:if test="${isSorted}">
		<h3>List of busy masters</h3>
	</c:if>
	<hr align="left" size="1" color="#ff0000" /><br>
	<table border="1" bordercolor="black" rules="all">
		<tr> 
			<td>Full name</td>
			<c:if test="${isSorted}">
				<td>Number of customs</td>
			</c:if>	
			<td></td>
	 	</tr>
		<c:forEach var="master" items="${masters}"> 
			<tr>
			    <td><c:out value="${master.fullName}"/></td>
				<c:if test="${isSorted}">
					<td><c:out value="${master.customCount}"/></td>
				</c:if>	
				<td><form action="delMaster" method="get">
					<input type="image" src="<c:url value="/resources/images/delIcon.jpg"/>"/>
					<input type="hidden" name="masterId" value="${master.id}">
				</form></td>
    		</tr>
		</c:forEach>  
	</table>
	<c:if test="${isSorted}">
		<p><a href="masters"> List of masters</a></p>
	</c:if>
	<c:if test="${!isSorted}">
		<p><a href="sortedMasters"> List of busy masters </a></p>
	</c:if>		
	<p><a href="addMaster"> Add master </a></p>
	<p><a href="*"> Home page </a></p>	
</body>
</html>