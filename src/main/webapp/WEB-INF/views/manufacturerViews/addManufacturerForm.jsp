<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%@ page language="java" contentType="text/html; charset=UTF-8;"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add manufacturer</title>
</head>
<body>
	<h3>Add manufacturer</h3>
	<hr align="left" size="1" color="#ff0000" />
	<p><b>Insert here manufacturer of device</b></p>
	<s:form action="addManufacturer" method="post" modelAttribute="manufacturer">
		<p><input type="text" name="name"></p>
		<p><input type="submit" value="Add"></p>
	 </s:form>
	 <p><a href="manufacturers"> List of manufacturers</a></p>
 	 <p><a href="*"> Home page </a></p>	
</body>
</html>