<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of customers</title>
</head>
<body>
	<c:if test="${!isSorted}">
		<h3>List of customers</h3> 
	</c:if>
	<c:if test="${isSorted}">
		<h3>List of customers with number of active customs</h3>
	</c:if>
	<hr align="left" size="1" color="#ff0000" /><br>
	<table border="1" bordercolor="black" rules="all">
		<tr> 
			<td>Full name</td>
			<td>Phone number</td>
			<c:if test="${isSorted}">
				<td>Number of active customs</td>
			</c:if>	
			<td></td>
	 	</tr>
		<c:forEach var="customer" items="${customers}"> 
			<tr>
			    <td><c:out value="${customer.fullName}"/></td>
			    <td><c:out value="${customer.phoneNumber}"/></td>
				<c:if test="${isSorted}">
					<td><c:out value="${customer.customCount}"/></td>
				</c:if>	
				<td><form action="delCustomer" method="get">
					<input type="image" src="<c:url value="/resources/images/delIcon.jpg"/>"/>
					<input type="hidden" name="customerId" value="${customer.id}">
				</form></td>
    		</tr>
		</c:forEach>  
	</table>
	<c:if test="${isSorted}">
		<p><a href="customers"> List of customers</a></p>
	</c:if>
	<c:if test="${!isSorted}">
		<p><a href="sortedCustomers"> List of customers with number of active custom</a></p>
	</c:if>			 
	<p><a href="*"> Home page </a></p>	
</body>
</html>