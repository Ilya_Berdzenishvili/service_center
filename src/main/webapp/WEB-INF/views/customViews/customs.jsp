<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of customs</title>
</head>
<body>
	<c:if test="${typeOfCustoms == 'All'}">
		<h3>List of all customs</h3>
	</c:if>		
	<c:if test="${typeOfCustoms == 'Completed'}">
		<h3>List of completed customs</h3>
	</c:if>		
	<c:if test="${typeOfCustoms == 'In process'}">
		<h3>List of not yet completed customs</h3>
	</c:if>	
	<c:if test="${typeOfCustoms == 'Faulty'}">
		<h3>List of faulty customs</h3>
	</c:if>	
	<c:if test="${typeOfCustoms == 'Failed'}">
		<h3>List of failed customs</h3>
	</c:if>	
	<c:if test="${typeOfCustoms == 'New'}">
		<h3>List of new customs</h3>
	</c:if>
	<hr align="left" size="1" color="#ff0000" /><br>
	<table border="1" bordercolor="black" rules="all">
		<tr> 
			<td>Customer</td>
			<td>Contacts</td>
			<td>Date</td>
			<td>Deadline</td>
			<td>Device</td>			
			<td>Manufacturer</td>
			<td>Guarantee</td>	
			<td>Type of crash</td>
			<td>State</td>
			<td>Master</td>
			<td></td>
			<td></td>
	 	</tr>
		<c:forEach var="custom" items="${customs}"> 
			<tr>
				<td><c:out value="${custom.customerFullName}"/></td>
				<td><c:out value="${custom.customerPhoneNumber}"/></td>
				<fmt:formatDate value="${custom.date}" var="start" type="date" pattern="yyyy-MM-dd"/>
				<td><c:out value="${start}"/></td>	
				<fmt:formatDate value="${custom.deadline}" var="end" type="date" pattern="yyyy-MM-dd"/>
				<td><c:out value="${end}"/></td>
				<td><c:out value="${custom.deviceType}"/></td>
				<td><c:out value="${custom.manufacturerName}"/></td>
				<c:if test="${custom.guarantee}">
					<td><c:out value="Yes"/></td>
				</c:if>
				<c:if test="${!custom.guarantee}">
					<td><c:out value="No"/></td>
				</c:if>	
				<td><c:out value="${custom.crashType}"/></td>
				<td><c:out value="${custom.state}"/></td>
				<td><c:out value="${custom.masterFullName}"/></td>
				<td><form action="editCustom" method="get">
					<input type="image" src="<c:url value="/resources/images/editIcon.jpg"/>"/>
					<input type="hidden" name="customId" value="${custom.id}">
				</form></td>
				<td><form action="delCustom" method="get">
					<input type="image" src="<c:url value="/resources/images/delIcon.jpg"/>"/>
					<input type="hidden" name="customId" value="${custom.id}">
				</form></td>
    		</tr>
		</c:forEach>  
	</table>  
	<p><b>Choose what would you like to watch</b></p>
	<ul style="list-style-type:disc">
		<c:if test="${typeOfCustoms == 'All'}">
			<li><a href="completedCustoms"> List of  completed customs </a></li>	
			<li><a href="notCompletedCustoms"> List of  not yet completed customs</a></li>
			<li><a href="faultyCustoms"> List of  faulty customs</a></li>
			<li><a href="failedCustoms"> List of  failed customs</a></li>
			<li><a href="newCustoms"> List of  of new customs</a></li>
		</c:if>		
		<c:if test="${typeOfCustoms == 'Completed'}">
			<li><a href="customs"> List of  all customs</a></li>	
			<li><a href="notCompletedCustoms"> List of  not yet completed customs</a></li>
			<li><a href="faultyCustoms"> List of  faulty customs</a></li>
			<li><a href="failedCustoms"> List of  failed customs</a></li>
			<li><a href="newCustoms"> List of  new customs</a></li>
		</c:if>		
		<c:if test="${typeOfCustoms == 'In process'}">
			<li><a href="customs"> List of  all customs</a></li>	
			<li><a href="completedCustoms"> List of  completed customs</a></li>
			<li><a href="faultyCustoms"> List of  faulty customs</a></li>
			<li><a href="failedCustoms"> List of  failed customs</a></li>
			<li><a href="newCustoms"> List of  new customs</a></li>
		</c:if>	
		<c:if test="${typeOfCustoms == 'Faulty'}">
			<li><a href="customs"> List of  all customs</a></li>	
			<li><a href="completedCustoms"> List of completed customs</a></li>
			<li><a href="notCompletedCustoms"> List of not yet completed customs</a></li>
			<li><a href="failedCustoms"> List of failed customs</a></li>
			<li><a href="newCustoms"> List of new customs</a></li>
		</c:if>
		<c:if test="${typeOfCustoms == 'Failed'}">
			<li><a href="customs"> List of all customs</a></li>	
			<li><a href="completedCustoms"> List of completed customs</a></li>
			<li><a href="notCompletedCustoms"> List of not yet completed customs</a></li>
			<li><a href="faultyCustoms"> List of faulty customs</a></li>
			<li><a href="newCustoms"> List of new customs</a></li>
		</c:if>	
		<c:if test="${typeOfCustoms == 'New'}">
			<li><a href="customs"> List of all customs</a></li>	
			<li><a href="completedCustoms"> List of completed customs</a></li>
			<li><a href="notCompletedCustoms"> List of not yet completed customs</a></li>
			<li><a href="faultyCustoms"> List of faulty customs</a></li>
			<li><a href="failedCustoms"> List of failed customs</a></li>
		</c:if>		
	</ul>
	<p><a href="addCustom"> Add custom </a></p>	 
	<p><a href="*"> Home page </a></p>	
</body>
</html>