<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%@ page import="test.service.com.entities.Custom"%>
<%@ page language="java" contentType="text/html; charset=UTF-8;"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add custom</title>
</head>
<body>
	<c:set var="action" value="addCustom"/>
	<c:if test="${!isForAdd}">
		<c:set var="action" value="editCustom"/>	
	</c:if>
	<h3>Add custom</h3>
	<hr align="left" size="1" color="#ff0000" />
	<s:form method="POST" action="${action}" modelAttribute="custom">
		<p>Customer's full name</p>
		<p><input type="text" name="customerFullName" value="${customerFullName}"/></p>
		<p>Customer's phone number</p>
		<p><input type="text" name="customerPhoneNumber" value="${customerPhoneNumber}"/></p>
		<p>Kind of device</p>
	    <s:select path="deviceId">
	        <c:if test="${isForAdd}">
				<s:option value="0" label=""/>
			</c:if>
            <s:options items="${devices}" itemValue="id" itemLabel="type"/>
       	</s:select>
		<p>Manufacturer of device</p>
	    <s:select path="manufacturerId">
	        <c:if test="${isForAdd}">
				<s:option value="0" label=""/>
			</c:if>
            <s:options items="${manufacturers}" itemValue="id" itemLabel="name"/>
       	</s:select>	
		<p>Short description of problem</p>
		<s:select path="crashId">
            <c:if test="${isForAdd}">
				<s:option value="0" label=""/>
			</c:if>
            <s:options items="${crashes}" itemValue="id" itemLabel="type"/>
       	</s:select>
		<p>Full description of problem</p>
		<p><s:input type="text" path="fullDescription"/></p>
		<c:if test="${guarantee}">
			<p>Is there any guarantee of device? <s:checkbox path="guarantee" checked="checked"/></p>
		</c:if>
		<c:if test="${!guarantee}">
			<p>Is there any guarantee of device? <s:checkbox path="guarantee"/></p>
		</c:if>
		<p>Required period of work(how many days?)</p>
		<c:set var="mili" value="${custom.deadline.getTime() - custom.date.getTime()}"/>
		<c:set var="days" value="${mili/(1000*60*60*24)}"/>	
		<fmt:formatNumber var="diff" type="number" value="${days}" maxFractionDigits="0"/>
		<c:if test="${!isForAdd}">
			<p><input type="text" name="term" value="${diff}"></p>
		</c:if>
		<c:if test="${isForAdd}">
			<p><input type="text" name="term"></p>
		</c:if>
		<p>Master for this custom</p>
        <s:select path="masterId" label="${masterName}">
        	<c:if test="${isForAdd}">
				<s:option value="0" label=""/>
			</c:if>
            <s:options items="${masters}" itemValue="id" itemLabel="fullName"/>
        </s:select>
        <c:if test="${!isForAdd}">
        	<p>Choose the state of this custom</p>
        	<s:select path="state" label="${state}">
        	    <s:option value="New" label="New"/>
				<s:option value="Completed" label="Completed"/>
				<s:option value="In process" label="In process"/>
				<s:option value="Faulty" label="Faulty"/>
        	</s:select>
		</c:if>
		<p><s:input type="hidden" path="id"/></p>
		<p><input type="submit" value="Checkout"></p>
	</s:form>
	<p><a href="customs"> List of customs</a></p>
	<p><a href="*"> Home page </a></p>	
</body>
</html>