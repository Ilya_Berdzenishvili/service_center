-- -----------------------------------------------------
-- Schema service
-- -----------------------------------------------------
drop table IF EXISTS `service`.custom;
drop table IF EXISTS `service`.master;
drop table IF EXISTS `service`.manufacturer;
drop table IF EXISTS `service`.device;
drop table IF EXISTS `service`.crash;
drop table IF EXISTS `service`.customer;
drop SCHEMA IF EXISTS `service`;

CREATE SCHEMA IF NOT EXISTS `service` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `service` ;

-- -----------------------------------------------------
-- Table `service`.`Master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`master` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fullName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `service`.`Manufacturer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`manufacturer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `service`.`Device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`device` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `service`.`Crash`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`crash` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `service`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fullName` VARCHAR(45) NOT NULL,
  `phoneNumber` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `service`.`Custom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `service`.`custom` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `deadline` DATE NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  `fullDescription` VARCHAR(45) NOT NULL,
  `guarantee` TINYINT(1) NOT NULL,
  `master_id` INT NOT NULL,
  `manufacturer_id` INT NOT NULL,
  `device_id` INT NOT NULL,
  `crash_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_custom_master_idx` (`master_id` ASC),
  INDEX `fk_custom_manufacturer_idx` (`manufacturer_id` ASC),
  INDEX `fk_custom_device_idx` (`device_id` ASC),
  INDEX `fk_custom_crash_idx` (`crash_id` ASC),
  INDEX `fk_custom_customer_idx` (`customer_id` ASC),
  CONSTRAINT `fk_custom_master`
    FOREIGN KEY (`master_id`)
    REFERENCES `service`.`master` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_custom_manufacturer`
    FOREIGN KEY (`manufacturer_id`)
    REFERENCES `service`.`manufacturer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_custom_device`
    FOREIGN KEY (`device_id`)
    REFERENCES `service`.`device` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_custom_crash`
    FOREIGN KEY (`crash_id`)
    REFERENCES `service`.`crash` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_custom_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `service`.`customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);